# Get the directory that this configuration file exists in
dir = File.dirname(__FILE__)

# Compass configurations
sass_path = dir
css_path = dir

# Require any additional compass plugins here.
images_dir = "."

output_style = :expanded
environment = :development
