<?php
if ($_POST) {
  $message = strtr("
    שם: fname lname
    Email: email
    טלפון: tel
    מיספר אורחים: parti
    ",$_POST);
  $to = 'tzvir@IsraelExperts.com , daphna@israelexperts.com , israelexperts.gibuy@gmail.com'; 
  mb_internal_encoding("UTF-8");
  mb_send_mail($to, 'landing page form', $message, "Mime-Version: 1.0\nContent-type: text/plain;charset=utf-8 \r\nReply-To: " . $_POST['email'] . "\r\n");
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta name="viewport" content="width=device-width">
<meta charset="UTF-8">
  <title></title>
<link href='http://fonts.googleapis.com/css?family=Crete+Round:400italic,400|Open+Sans:300,600' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="style.css">
</head>
<body class="<?php echo ($_GET['thankyou'] ? 'thankyou' : 'regular') ; ?>">
<header>
  <a class="logo-beitinu" href="beitinu-site"><img src="beitinu-logo.png" alt=""></a>
  <a class="logo-ie" href="https://israelexperts.co.il"><img src="logo-ie.svg" alt=""></a>
  <div class="so">
    <a class=in target=_blank href="http://instagram.com/israelexperts">instagram</a>
    <a class=tw target=_blank href="https://twitter.com/TaglitIsraelExp">twitter</a>
    <a class=fb target=_blank href="https://www.facebook.com/TaglitIsraelExperts">facebook</a>
  </div>
</header>
<?php if ($_GET['thankyou']): ?>
<div class="thankyou">
  <h1>THANK YOU!</h1>
  <p>Thank yu for your interest, We received your request and will call you back soon.</p>
</div>
<script>setTimeout(function() { location.href = "http://www.israelexperts.com/" }, 4000); </script>
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 1025640144;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "ffffff";
var google_conversion_label = "xB6FCNHt-1wQ0I2I6QM";
var google_remarketing_only = false;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/1025640144/?label=xB6FCNHt-1wQ0I2I6QM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
<?php else: ?>
  <div>
    <h2>Travel to Israel with Rabbi Cove and the "Beiteinu" Community December 24, 2015 – January 2, 2016</h2>
    <p>They say that you never really know a place until you have been there.
This is your opportunity to get to know Israel for what it really is - a world class cultural and religious destination. No matter your current view of the people and the land, every step you take, every conversation you have and every site you encounter will tell a story. Each tale will remind you this is a very dynamic, remarkable and special place.</p>
  </div>

<form action="?thankyou=1" method=post>
  <h2>Leave your info, we’ll do the rest</h2>
  <input required id=form type="text" name=fname placeholder="First Name">
  <input required type="text" name=lname placeholder="Last Name">
  <input type="number" name=parti placeholder="Number of Participants in Party">
  <input required type="email" name=email placeholder="Email Address">
  <input required type="tel" name=phone placeholder="Telephone Number">
  <button type="submit">JOIN US</button>
</form>
<main>
<section>
  <p> Our Israel experience will allow you to have conversations with people across Israeli society and get to know the country from up close.  </p>
  <p> Here are just a few examples from the program: </p>
  <ul>
    <li><b>"Bread and Butter"</b> walking tour of Jaffa lead by Jewish and Arab co-guides, focusing on the role of bread in the three Abrahamic religions.  </li>
    <li><b>Theater performance</b> by visually impaired actors at the Na Lagaat Theatre</li>
    <li><b>Evening viewing of the three-dimensional audiovisual presentation</b> at Beit Shean National Park </li>
    <li><b>Explore the Ein Shemer Greenhouse for Peace and Coexistence</b>; meet the Jewish and Arab students who create, learn, and grow in the greenhouse.</li>
    <li><b>Enjoy a Shabbat dinner</b> with Israeli lone soldiers</li>
    <li><b>Tour the mystical city of Safed</b> and its renowned artist quarter</li>
    <li><b>Marvel at Jerusalem's renovated Jewish Quarter</b> and the Western Wall</li>
    <li><b>Go deep underground</b> to the hidden Machon Ayalon munitions factory</li>
  </ul>
  <p>Beiteinu, the new house in the Jewish neighborhood of Philadelphia, is committed to bring the joy of Jewish life to the places and spaces where people are.</p>
  <p>Rabbi Cove and Beiteinu are committed to fostering communication and understanding between Jews and people of other faiths. </p>
  <a href="#form">CLICK HERE FOR TRIP ITINERARY</a>
  <h3> For additional information regarding trip, please contact rabbiofbeiteinu@gmail.com </h3>
</section>

<h2> Program Itinerary </h2>

<h3>
  <span>Day 1: Thursday, December 24th</span>
  <span>Welcome to Israel!</span>
</h3>
<ul>
  <li>Arrive in Israel – (For those on a group flight, you will be met at the airport by your guide and an IsraelExperts representative).</li></li>
  <li>Shehechyanu prayer on the beach promenade – what these shores have seen – engage in an introductory program that contemplates some key historical moments</li></li>
  <li>Check-in your hotel in Tel Aviv </li>
  <li>Dinner at Black Out at the Na Lagaat Theatre (restaurant and theatre troupe comprised entirely of visual impaired staff)</li>
  <li>Na Lagaat Theatre  (TBD if they have shows)</li>
  <li>Overnight: Tel Aviv Dinner</li>
</ul>

<h3>
  <span>Day 2:  Friday, December 25th</span>
  <span>The Ongoig Challenge – Making Community, Making Peace</span>
</h3>
<ul>
  <li>Breakfast at the hotel </li>
  <li>Orientation with your guide and Rabbi Cove </li>
  <li>Start your trip with a visit to Independence Hall where David Ben-Gurion proclaimed Israel's Independence in 1948. Learn about the circumstances under which the State of Israel was established before discovering how it has evolved. </li>
  <li>"Bread and Butter" - walking tour of Jaffa, one of the world's ancient ports, focusing on the role of bread in the three Abrahamic religions.  Jewish and Arab co-guides will lead this tour of local bakeries and historical sites, narrating the struggles and successes of co-existence. The many bakery samples will satisfy everyone's appetite for lunch.</li>
  <li>Browse the nearby Nachalat Binyamin art fair and enjoy the typical Tel Aviv afternoon scene. </li>
  <li>Shabbat service with a local congregation at Beit Tefilah Israeli Tel Aviv </li>
  <li>Shabbat dinner at the hotel  (main dining room)</li>
  <li>Overnight: Tel Aviv     Breakfast, Lunch, and Dinner </li>
</ul>

<h3>
  <span>Day 3: Saturday, Dec 26th</span>
  <span>Kodesh VeChol – Shabbat in Tel Aviv</span>
</h3>
<ul>
  <li>Breakfast at the hotel </li>
  <li>Optional: join Shabbat services on beach (weather permitting) or with a local congregation </li>
  <li>Text Study with Rabbi Cove </li>
  <li>Lunch on own in Rothschild area </li>
  <li>Historical and architectural walking tour of Tel Aviv (rain plan: Ben Gurion's home) </li>
  <li>Encounter with "Olim Vatikim" – American immigrants to Israel, who will share experiences from their 35 years in the country</li>
  <li>Havdalah Service (on the beach, weather permitting) – welcoming the new week. </li>
  <li>Evening on own in Tel Aviv – Tour the Neve Tzedek area or the new Tel Aviv Port, the “Station” (HaTachana) or the Sarona area. </li>
  <li>Overnight: Tel Aviv         Breakfast</li>
</ul>


<h3>
  <span>Day 4: Sunday, Dec 27th</span>
  <span>Building a Nation: Challenges Then and Now</span>
</h3>
<ul>
  <li>Late breakfast at the hotel and check-out </li>
  <li>Visit Rabin Square ("Kikar Rabin" in Hebrew), where Prime Minister Rabin was assassinated at the conclusion of a massive peace rally held at the square on November 4, 1995. Contemplate the effects of the murder of the man who more than all symbolized peace. </li>
  <li>Tour historic, 19th century moshava of Zichron Yaakov.  Hear about its pioneers, scholars, and spies.</li>
  <li>Lunch on your own in one of the picturesque cafes in Zichron </li>
  <li>Meet the staff and students at the Ein Shemer Greenhouse for Peace and Co-Existence and experience the outstanding work carried out there with the founder, Avital Geva.</li>
  <li>Group Dinner at a "Steakiya" in Beit Shean</li>
  <li>Enjoy an evening visit to Beit Shean National Park where you will view a three-dimensional audiovisual presentation. You can almost taste the spices and inhale the aromas in Bet She’an’s ancient markets as you meet characters from across the Fertile Crescent as well as those who brought it to life after 2,000 years. </li>
  <li>Drive north to kibbutz guest house in the Upper Galilee  </li>
  <li>Overnight: Kibbutz in the North   Breakfast and Dinner</li>
</ul>

<h3>
  <span>Day 5: Monday, Dec 28th</span>
  <span>Mystic & Magnificent</span>
</h3>
<ul>
  <li>Breakfast at the hotel</li>
  <li>Visit Tzfat (Safed) and explore this fascinating city regarded one of the four Holy cities in Judaism along with Jerusalem, Tiberias and Hebron. Tzfat is a center for the Kabbalah, an esoteric form of Jewish mysticism. Tour together the ancient synagogues and artists’ square and meet with Avraham Loewenthal, a local artist </li>
  <li>Lunch on your own in Tzfat</li>
  <li>Tree planting with JNF, near the Golani Intersection</li>
  <li>Tour the Kinneret Cemetery: learn about the poets, soldiers, dreamers, and laborers who built the country </li>
  <li>Dinner at the hotel (main dining room) with two Israeli soldiers, who will share their experiences and perspectives (TBC and may join the group in Tel Aviv isntead)</li>
  <li>Overnight: Kibbutz in the North   Breakfast and Dinner </li>
</ul>

<h3>
  <span>Day 6: Tuesday, Dec 29th</span>
  <span>Higher and Lower: From the Golan to the Dead Sea</span>
</h3>
<ul>
  <li>Breakfast at hotel </li>
  <li>Drive up to the Golan Heights and enjoy the panoramic view from Mount Bental, overlooking Mount Hermon and into Syria. Discuss the strategic importance of the Golan Heights and the borders with Israel’s neighbors. </li>
  <li>Visit the reconstructed Talmudic Village of Katzrin.  See the ancient ruins and learn about everyday life in first few centuries CE</li>
  <li>Lunch on own at Katzrin</li>
  <li>Visit the Golan Heights Winery for a tour and wine tasting </li>
  <li>Drive down Jordan Valley toward hotel in Dead Sea.  Learn about Israel's largest and quietest border – its border with Jordan</li>
  <li>Arrive at the Hotel for check in and dinner (main dining room)</li>
  <li>Overnight: Dead Sea         Breakfast and Dinner</li>
</ul>

<h3>
  <span>Day 7: Wednesday, Dec 30th</span>
  <span>The Masada Narrative</span>
</h3>
<ul>
  <li>Breakfast at the hotel </li>
  <li>Ein Gedi water hike</li>
  <li>Ein Gedi Botanical Gardens or Qumran Caves </li>
  <li>Lunch on own </li>
  <li>Return to the hotel and enjoy a “swim” in the Dead Sea and the hotel’s spa facilities </li>
  <li>Dinner at the hotel (main dining room)</li>
  <li>Film and Discussion (Recommended film: Checkpoint - http://www.maale.co.il/default.asp?PageID=73&ItemID=217&ItemName=Barriers) </li>
  <li>Overnight: Dead Sea         Breakfast and Dinner </li>
</ul>

<h3>
  <span>Day 8: Thursday, Dec 31st</span>
  <span>The Holiness of History and the History of Holiness</span>
</h3>
<ul>
  <li>Early Breakfast at the hotel </li>
  <li>Take the cable car up (and down) Massada, a symbol of the ancient kingdom of Israel with its violent destruction and the last stand of Jewish Zealots in the face of the Roman army, in 73 CE. Join Rabbi Cove for morning prayers on the summit of Massada.</li>
  <li>Continue up to the holy city of Jerusalem, with a stop on Mt. Scopus for a panoramic view of the city and a Shehechyanu prayer with Rabbi Cove. </li>
  <li>Enter the walled city and walk through the Armenian Quarter to the newly restored Jewish Quarter. </li>
  <li>Lunch on your own in one of the many Jewish Quarter restaurants</li>
  <li>Tour the Roman Cardo, and the four Sephardic synagogues restored after the 1967 Six-Day war in the Jewish Quarter. Visit the Kotel – the Western Wall.</li>
  <li>Arrive at your Jerusalem hotel  </li>
  <li>Dinner on own in Jerusalem  </li>
  <li>Overnight: Jerusalem      Breakfast and Dinner</li>
</ul>


<h3>
  <span>Day 9: Friday, January 1st</span>
  <span>Our Shared World</span>
</h3>
<ul>
  <li>Breakfast at the hotel </li>
  <li>Visit Yad Vashem, Israel's national memorial to the Holocaust, which presents the historic events that befell Europe following the rise of Nazism rise to power in Germany, and the fate of the Jews under Nazi rule. </li>
  <li>Ascend from Yad Vashem to Mt. Herzl National Cemetery.  Hear the stories of those who made the ultimate sacrifice for the State of Israel</li>
  <li>Visit the Machne Yehuda Shuk (Market) to take in the sights and smells of Jerusalem preparing for Shabbat. Enjoy lunch on your own at one of the many restaurants or stands and shop for snacks for Oneg Shabbat. </li>
  <li>Kabbalat Shabbat with Rabbi Cove</li>
  <li>Shabbat dinner at hotel  (main dining room)</li>
  <li>Oneg Shabbat</li>
  <li>Overnight: Jerusalem        Breakfast and Dinner </li>
</ul>


<h3>
  <span>Day 10: Saturday, January 2nd</span>
  <span>Shabbat – An Island in Time and in Space</span>
</h3>
<ul>
  <li>Breakfast at the hotel </li>
  <li>Morning program on the Changing Map of the Middle East.  Engage in a lively discussion during this interactive program that focuses on political and historical realities that have shaped the geography of the Middle East.  </li>
  <li>Visit the Burma Road to learn about the role of Jews from the Diaspora in the narrative of modern Israel with a discussion of the current key challenges.</li>
  <li>Box lunch</li>
  <li>Machon Ayalon: site of a once-secret munitions factory that existed during the British Mandate Period </li>
  <li>Lunch on own in the Old City </li>
  <li>Group havdalah</li>
  <li>Final dinner and closing program </li>
  <li>Late Check Out (after Shabbat)</li>
  <li>Transfer to BenGurion International Airport for fight home. </li>
  <li>Breakfast, Lunch, and Dinner</li>
</ul>

</main>
<?php endif; ?>                         
</body>                                 
<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-13204786-7', 'auto');
ga('send', 'pageview');

</script>
</html>
